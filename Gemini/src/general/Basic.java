package general;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Basic {

	static WebDriver driver;
	public static Properties prop;

	public static void goTO(String url) {
		if (url.equals("/")) {
			driver.navigate().to(prop.getProperty("url"));

		} else {
			driver.get(url);
		}
	}
	
	public static void wait(String abc)
	
	{
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(abc)));
	}

	public static void refresh() {
		driver.navigate().refresh();

	}

	public static void click(String abc) {

		wait(abc);
		
		/*WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(abc)));*/
		WebElement element = driver.findElement(By.xpath(abc));
		element.click();
	}
	
	
	public static void verifyText(String Text) {		
		
		WebElement element=driver.findElement(By.xpath("Locator Value"));
		String text=element.getText();
		if(text.equals(Text)){

			System.out.println("Expected text is obtained");

			}
		else{

			System.out.println("Expected text is not obtained");
	}}
	
	public static void main (String args[]) throws IOException {
		
		
		 prop = new Properties();
			FileInputStream ip = new FileInputStream("D:\\Automation\\Test\\Gemini\\src\\general\\config.properties");
			prop.load(ip);
		
		
		String exePath = "D:\\Automation\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		driver.get(prop.getProperty("url"));
		goTO("/");
		
	}

}
